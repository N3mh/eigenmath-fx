/*
	Console Lib for CASIO fx-9860G
							by Mike Smith.
*/

#ifndef CONSOLE_H
#define CONSOLE_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdlib.h>
#include <string.h>
#include "fxlib.h"

	enum CONSOLE_SCREEN_SPEC{
		LINE_MAX = 32,
		LINE_DISP_MAX = 7,
		COL_DISP_MAX = 21,//21
		EDIT_LINE_MAX = 2048
	};

	enum CONSOLE_RETURN_VAL{
		CONSOLE_NEW_LINE_SET = 1,
		CONSOLE_SUCCEEDED = 0,
		CONSOLE_MEM_ERR = -1,
		CONSOLE_ARG_ERR = -2,
		CONSOLE_NO_EVENT = -3
	};

	enum CONSOLE_CURSOR_DIRECTION{
		CURSOR_UP,
		CURSOR_DOWN,
		CURSOR_LEFT,
		CURSOR_RIGHT
	};

	enum CONSOLE_LINE_TYPE{
		LINE_TYPE_INPUT,
		LINE_TYPE_OUTPUT
	};

	enum CONSOLE_CASE{
		LOWER_CASE,
		UPPER_CASE
	};

	struct line{
		unsigned char *str;
		unsigned char *tex_str;
		int readonly;
		int type;
		int start_col;
		int disp_len;
		unsigned char tex_flag;
		int tex_height, tex_width;
	};

	struct FMenu{
		char* name;
		char** str;
		unsigned char count;
	};

	struct location{
		int x;
		int y;
	};

#define MAX_FMENU_ITEMS 7
#define FMENU_TITLE_LENGHT 4

#define is_wchar(c) ((c == 0x7F) || (c == 0xF7) || (c == 0xF9) || (c == 0xE5) || (c == 0xE6) || (c == 0xE7))
#define printf(s) Console_Output((const unsigned char *)s);

	int Console_DelStr(unsigned char *str, int end_pos, int n);
	int Console_InsStr(unsigned char *dest, const unsigned char *src, int disp_pos);
	int Console_GetActualPos(const unsigned char *str, int disp_pos);
	int Console_GetDispLen(const unsigned char *str);
	int Console_MoveCursor(int direction);
	int Console_Input(const unsigned char *str);
	int Console_Output(const unsigned char *str);
	int Console_Clear_EditLine();
	int Console_NewLine(int pre_line_type, int pre_line_readonly);
	int Console_Backspace(void);
	int Console_GetKey(void);
	int Console_Init(void);
	int Console_Disp(void);
	int Console_FMenu(int key);
	void Console_FMenu_Init(void);
	int Console_Draw_FMenu(int key, struct FMenu* menu);
	char *Console_Make_Entry(const unsigned char* str);
	unsigned char *Console_GetLine(void);
	unsigned char* Console_GetEditLine();
	void Console_Draw_TeX_Popup(unsigned char* str, int width, int height);

#ifdef __cplusplus
}
#endif
#endif